package dataLayer;

import java.io.*;
import java.util.*;
import bussinesLayer.*;

public class RestaurantSerializator implements Serializable {
	
	
	public void serializationMenuItem (ArrayList<MenuItem> item)
	{	
		  try {
		         FileOutputStream fileOut =
		         new FileOutputStream("restaurant.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(item);
		         out.close();
		         fileOut.close();
		         System.out.printf("Serialized data is saved in Restaurant.ser");
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
   }
	
	
	public ArrayList<MenuItem> deserializationMenuItem(){

		ArrayList<MenuItem> items= new ArrayList<MenuItem>();
     try {
        FileInputStream fileIn = new FileInputStream("restaurant.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        items = (ArrayList<MenuItem>) in.readObject();
        in.close();
        fileIn.close();
        return items;
     } catch (IOException i) {
        i.printStackTrace();
        return null;
     } catch (ClassNotFoundException c) {
        System.out.println("not found");
        c.printStackTrace();
        return null;
     }
	}
	
	
	
	public void serializationOrders (ArrayList<Order> order)
	{	
		  try {
		         FileOutputStream fileOut =
		         new FileOutputStream("Order.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(order);
		         out.close();
		         fileOut.close();
		         System.out.printf("Serialized data is saved in  Order.ser");
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
   }
	
	
	public ArrayList<Order> deserializationOrders(){

		ArrayList<Order> orders= new ArrayList<Order>();
     try {
        FileInputStream fileIn = new FileInputStream("Order.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        orders = (ArrayList<Order>) in.readObject();
        in.close();
        fileIn.close();
        return orders;
     } catch (IOException i) {
        i.printStackTrace();
        return null;
     } catch (ClassNotFoundException c) {
        System.out.println("not found");
        c.printStackTrace();
        return null;
     }
	}
	
	
	
	

}
