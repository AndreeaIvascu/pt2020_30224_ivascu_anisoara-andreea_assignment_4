package dataLayer;

import java.io.PrintWriter;
import java.util.ArrayList;

import bussinesLayer.BaseProduct;
import bussinesLayer.CompositeProduct;
import bussinesLayer.MenuItem;
import bussinesLayer.Order;

public class FileWriter {

	
	
	
	
	public void  chitanta(Order order)
	{
		 ArrayList<MenuItem> items=order.getItem();     
		   
			try {
				PrintWriter output = new PrintWriter("BillNo" + order.getOrderID() + ".txt");
				output.println("--NOTA DE PLATA--");
				output.println();
				output.println("ora: "+order.getDateHour()+":00"+" minut: "+order.getDateMin());
	            //output.println();
				output.println("Masa "+order.getTable());
				output.println("Comanda " + order.getOrderID() + " are o suma de plata "+order.getPrice());
				output.println();
				output.println("Produsele comandate sunt:");
				output.println();
				
				for(int i=0;i<items.size();i++)
					
				{
					if(items.get(i) instanceof BaseProduct)
					output.println("produs: "+((BaseProduct) items.get(i)).getNumeBaseProd()+" -> pret: "+((BaseProduct) items.get(i)).getPretBaseProd());
					if(items.get(i) instanceof CompositeProduct)
						output.println("produs: "+((CompositeProduct) items.get(i)).getNumeCompProd()+" -> pret: "+((CompositeProduct) items.get(i)).getPretCompProd());
				}
				output.close();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
	}
}
