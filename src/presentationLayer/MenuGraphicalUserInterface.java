package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MenuGraphicalUserInterface extends JFrame implements ActionListener {

	public JFrame jFrame;
	public JPanel panel1;
	public JPanel panel2;
	public JPanel panel3;
	public JPanel panel4;
	public JLabel labelChef;
	public JLabel labelWaiter;
	public JLabel labelAdministrator;
	public JLabel labelMeniuPrincipal;
	public JButton btnWaiter;
	public JButton btnAdministrator;
	public JButton btnChef;

	public MenuGraphicalUserInterface() {
		
		labelMeniuPrincipal = new JLabel(" Meniu Principal");
		btnAdministrator = new JButton("Administrator");
		btnWaiter = new JButton("Waiter");
		btnChef = new JButton("Chef");

		labelAdministrator = new JLabel("Optiuni Acces: Administrator, ");
		labelWaiter = new JLabel(" Waiter, ");
		labelChef = new JLabel(" Chef ");
		
		panel2=new JPanel();
		panel1=new JPanel();
		panel3=new JPanel();
		panel4=new JPanel();

		panel2.add(btnAdministrator);
		panel2.add(btnWaiter);
		panel2.add(btnChef);

		panel1.add(labelAdministrator);
		panel1.add(labelWaiter);
		panel1.add(labelChef);
		panel4.add(labelMeniuPrincipal);
		
		panel3.add(panel4);
		panel3.add(panel1);
		panel3.add(panel2);
		
		
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));
		panel4.setLayout(new BoxLayout(panel4, BoxLayout.Y_AXIS));
		
		jFrame = new JFrame("Meniu Principal");
	//	jFrame.add(panel4);
		jFrame.add(panel3);
		jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		jFrame.setSize(600, 200);
		jFrame.setVisible(true);

       action();

	
	}
	public void action() {
		btnAdministrator.addActionListener(this);
		btnWaiter.addActionListener(this);
		btnChef.addActionListener(this);
	}
		

	@Override

		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource()==btnAdministrator)
			{
				new AdministratorGraphicalUserInterface();
			}
			if(e.getSource()==btnWaiter)
			{
				new WaiterGraphicalUserInterface();
			}
			if(e.getSource()==btnChef)
			{
				new ChefGraphicalUserInterface();
			}
		}

	}


