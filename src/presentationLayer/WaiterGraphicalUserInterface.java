package presentationLayer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import bussinesLayer.BaseProduct;
import bussinesLayer.CompositeProduct;
import bussinesLayer.MenuItem;
import bussinesLayer.Order;
import bussinesLayer.Restaurant;
import dataLayer.RestaurantSerializator;

public class WaiterGraphicalUserInterface extends JFrame implements ActionListener {

	JFrame jFrame;
	JPanel panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8, panel9,panel10;
	JLabel labelWaiter, optiuniWaiter, labelOrderID, labelInformatii, labelProduseComp, labelProduseBase,labelTable,
			labelVizualizare, labelData, labelInfo1, labelInfo2;
	JButton addNewOrder, computeBill, viewAllOrders, viewMeniu;
	JTextField orderID,orderTable, detalii, produseBase, produseComp;
	Restaurant restaurant = new Restaurant();
	RestaurantSerializator ser = new RestaurantSerializator();

	public WaiterGraphicalUserInterface() {

		labelWaiter = new JLabel(" Ati intrat in meniul ospatarului!");
		addNewOrder = new JButton("addNewOrder");
		computeBill = new JButton("computeBill");
		viewAllOrders = new JButton("viewAllOrders");
		viewMeniu = new JButton("viewMenu");
		orderID = new JTextField(5);
		detalii = new JTextField(10);
		orderTable = new JTextField(10);
		produseBase = new JTextField(20);
		produseComp = new JTextField(20);
		labelOrderID = new JLabel("ID-ul comenzii:");
		labelInformatii = new JLabel("informatii comanda:");
		labelProduseComp = new JLabel("Composite Product-uri:");
		labelProduseBase = new JLabel("Base Product-uri:");
		labelVizualizare = new JLabel("Pentru efectuarea unei comenzi,vizualizati meniul!");
		labelData = new JLabel("(Ex:ora,minut)");
		labelInfo1 = new JLabel("(Ex:nume,pret)");
		labelInfo2 = new JLabel("(Ex:nume,pret)");
		labelTable = new JLabel("Masa: ");

		optiuniWaiter = new JLabel(" Optiunile Ospatarului sunt urmatoarele ");

		panel10=new JPanel();
		panel9 = new JPanel();
		panel8 = new JPanel();
		panel7 = new JPanel();
		panel6 = new JPanel();
		panel5 = new JPanel();
		panel4 = new JPanel();
		panel3 = new JPanel();
		panel2 = new JPanel();
		panel1 = new JPanel();

		panel5.add(labelOrderID);
		panel5.add(orderID);

		panel6.add(labelInformatii);
		panel6.add(detalii);
		panel6.add(labelData);

		panel7.add(labelProduseBase);
		panel7.add(produseBase);
		panel7.add(labelInfo1);

		panel8.add(labelProduseComp);
		panel8.add(produseComp);
		panel8.add(labelInfo2);

		panel9.add(labelVizualizare);
		panel10.add(labelTable);
		panel10.add(orderTable);

		panel2.add(addNewOrder);
		panel2.add(computeBill);
		panel2.add(viewAllOrders);
		panel2.add(viewMeniu);

		panel4.add(labelWaiter);
		panel1.add(optiuniWaiter);

		panel5.setLayout(new FlowLayout());
		panel6.setLayout(new FlowLayout());
		panel7.setLayout(new FlowLayout());
		panel8.setLayout(new FlowLayout());
		panel10.setLayout(new FlowLayout());

		panel3.add(panel4);
		panel3.add(panel1);
		panel3.add(panel2);
		panel3.add(panel9);
		panel3.add(panel5);
		panel3.add(panel10);
		panel3.add(panel6);
		panel3.add(panel7);
		panel3.add(panel8);

		panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));

		jFrame = new JFrame("Waiter");
		jFrame.add(panel3);
		jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		jFrame.setSize(600, 300);
		jFrame.setVisible(true);

		action();
	}

	public void action() {
		addNewOrder.addActionListener(this);
		computeBill.addActionListener(this);
		viewAllOrders.addActionListener(this);
		viewMeniu.addActionListener(this);

	}

	public ArrayList<MenuItem> add()

	{
		ArrayList<MenuItem> items = new ArrayList<MenuItem>();

		try {
			String comandaBase = produseBase.getText();
			String[] produseBase = comandaBase.split(",");
			String numeB = "";
			double pretB;
			for (int i = 0; i < produseBase.length; i = i + 2) {
				BaseProduct prod1 = new BaseProduct();
				numeB = produseBase[i];
				pretB = Double.parseDouble(produseBase[i + 1]);
				prod1.setNumeBaseProd(numeB);
				prod1.setPretBaseProd(pretB);
				items.add(prod1);
			}

			String comandaComp = produseComp.getText();
			String[] produseComp = comandaComp.split(",");
			String numeC = "";
			double pretC;
			for (int i = 0; i < produseComp.length; i = i + 2) {
				CompositeProduct prod2 = new CompositeProduct();
				numeC = produseComp[i];
				pretC = Double.parseDouble(produseComp[i + 1]);
				prod2.setNumeCompProd(numeC);
				prod2.setPretCompProd(pretC);
				items.add(prod2);
			}
		} catch (Exception e) {
			if (produseBase.getText().isEmpty() || produseComp.getText().isEmpty()) {
				System.out.println("Editati produse in ambele casete text!!!");
				// return items;
			}
		}
		return items;
	}

	public void addOrder() {
		String ID = orderID.getText();
		String masa = orderTable.getText();
		String info = detalii.getText();
		String[] informatii = info.split(",");
		int ora = Integer.parseInt(informatii[0]);
		int min = Integer.parseInt(informatii[1]);
		int id = Integer.parseInt(ID);
		int table = Integer.parseInt(masa);
		Order order = new Order();
		ArrayList<MenuItem> item = new ArrayList();
		item = add();
		order.setItem(item);
		order.setDateHour(ora);
		order.setDateMin(min);
		order.setOrderID(id);
		order.setTable(table);
		
		restaurant.createNewOrder(order);

	}

	public void view() {

		String[] id = new String[20];
		String[] ora = new String[20];
		String[] minut = new String[20];
		String[][] data = new String[10][5];

		int i1 = 0;

		ArrayList<Order> orders;
		orders = ser.deserializationOrders();
		restaurant.setOrders(orders);

		for (int i = 0; i < orders.size(); i++) {
			ArrayList<MenuItem> items = orders.get(i).getItem();
			data[i1][0] = "ID: " + orders.get(i).getOrderID();
			data[i1][1] = "" + orders.get(i).getDateHour() + ":00h";
			data[i1][2] = "" + orders.get(i).getDateMin() + "min";
			data[i1][3] = "" + orders.get(i).getTable() + " table";
			data[i1][4] = "" + items.toString();

			i1++;
		}

		setLayout(new FlowLayout());
		String[] column = { "orderID", "orderHour", "orderMin", "masa","componente" };
		JTable table = new JTable(data, column);
		table.setPreferredScrollableViewportSize(new Dimension(400, 100));

		table.setFillsViewportHeight(true);

		JScrollPane jps = new JScrollPane(table);
		add(jps);
		JFrame jf = new JFrame("Orders");

		jf.setSize(500, 500);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(1);
		jf.add(table);

	}

	private void computeBill() {
		String idOrd = orderID.getText();
		int id = Integer.parseInt(idOrd);

		restaurant.setOrders(ser.deserializationOrders());
		ArrayList<Order> orders = restaurant.getOrders();

		for (int i = 0; i < orders.size(); i++) {
			if (orders.get(i).getOrderID() == id) {
				restaurant.generateBill(orders.get(i));
			}
		}

	}

	public void viewMeniu() {
		String[] nume = new String[20];
		String[] pret = new String[20];
		String[][] data = new String[20][3];

		setLayout(new FlowLayout());

		JLabel labelProduse = new JLabel("Inainte de a adauga un produs consultati meniul!!");
		JPanel panel1 = new JPanel();

		panel1.add(labelProduse);

		int i1 = 0;

		ArrayList<MenuItem> items;
		items = ser.deserializationMenuItem();
		restaurant.setItems(items);

		for (int i = 0; i < items.size(); i++) {
			if (items.get(i) instanceof BaseProduct) {

				nume[i] = ((BaseProduct) items.get(i)).getNumeBaseProd();
				pret[i] = "" + ((BaseProduct) items.get(i)).getPretBaseProd();
				data[i1][0] = nume[i];
				data[i1][1] = pret[i] + " RON";
				data[i1][2] = "baseProduct";
				i1++;
			}
			if (items.get(i) instanceof CompositeProduct) {
				nume[i] = ((CompositeProduct) items.get(i)).getNumeCompProd();
				pret[i] = "" + ((CompositeProduct) items.get(i)).getPretCompProd();
				data[i1][0] = nume[i];
				data[i1][1] = pret[i] + " RON";
				data[i1][2] = "compositeProduct";
				i1++;
			}
		}
		JFrame jf = new JFrame("--Meniu--");
		String[] columnNames = { "nume", "pret", "specificatie" };
		JTable table;

		table = new JTable(data, columnNames);
		table.setPreferredScrollableViewportSize(new Dimension(450, 63));

		table.setFillsViewportHeight(true);

		JScrollPane jps = new JScrollPane(table);
		add(jps);

		jf.add(panel1);
		jf.add(table);
		jf.setDefaultCloseOperation(1);
		jf.setSize(600, 600);
		jf.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (e.getSource() == addNewOrder) {

			addOrder();
		}
		if (e.getSource() == computeBill) {
			computeBill();
		}
		if (e.getSource() == viewAllOrders) {
			view();

		}
		if (e.getSource() == viewMeniu) {
			viewMeniu();

		}

	}

}
