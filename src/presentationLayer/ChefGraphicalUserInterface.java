package presentationLayer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import bussinesLayer.BaseProduct;
import bussinesLayer.CompositeProduct;
import bussinesLayer.MenuItem;
import bussinesLayer.Order;
import bussinesLayer.Restaurant;
import dataLayer.RestaurantSerializator;

public class ChefGraphicalUserInterface extends JFrame implements ActionListener, Observer {

	public JFrame jFrame;
	public JPanel panel1;
	public JPanel panel2;
	public JPanel panel3,panel4;
	public JLabel labelChef;
	public JButton btnChef;
	public JButton btnNotify;
	public JTextArea area;
	Restaurant restaurant = new Restaurant();
	RestaurantSerializator ser = new RestaurantSerializator();
	ArrayList<MenuItem> items = new ArrayList<MenuItem>();

	public ChefGraphicalUserInterface() {

		btnChef = new JButton("Chef");
		btnNotify = new JButton("Notify");
		
		area=new JTextArea(5,20);
		JScrollPane sp=new JScrollPane(area);
		area.setEditable(false);

		labelChef = new JLabel(" Ati intrat in meniul bucatarului!");

		panel2 = new JPanel();
		panel1 = new JPanel();
		panel3 = new JPanel();
		panel4=new JPanel();

		//panel2.add(btnChef);
		panel2.add(btnNotify);

		panel1.add(labelChef);
       panel4.add(area);
		panel3.add(panel1);
		panel3.add(panel2);
		panel3.add(panel4);

		
		
		panel2.setLayout(new FlowLayout());
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));

		jFrame = new JFrame("Chef");

		jFrame.add(panel3);
		jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		jFrame.setSize(600, 200);
		jFrame.setVisible(true);

		action();

	}

	public void action() {

		btnChef.addActionListener(this);
		btnNotify.addActionListener(this);
	}
	public void view()
	{   
		String s="";
		
         ArrayList<Order> orders=new ArrayList<Order>();
         orders=ser.deserializationOrders();
         for(int i=0;i<orders.size();i++) {
        
        
			ArrayList<MenuItem> items = orders.get(i).getItem();
		area.setText( "ID Comanda"+orders.get(i).getOrderID());
			s = "" + items.toString();
			area.setText(s);

         }
         
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		items= ser.deserializationMenuItem();
		System.out.println("Notify");
		ArrayList<MenuItem> list = items;

	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

        if(e.getSource()==btnNotify)
        {
            update(null,items);
            view();
        }
        

        if(e.getSource()==btnChef)
        {
            
        }

	}

}
