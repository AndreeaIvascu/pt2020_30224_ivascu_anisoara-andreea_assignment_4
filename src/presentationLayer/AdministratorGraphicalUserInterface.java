package presentationLayer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

//import com.sun.tools.javac.util.Name.Table;

import bussinesLayer.*;
import dataLayer.RestaurantSerializator;

public class AdministratorGraphicalUserInterface extends JFrame implements ActionListener {

	Restaurant restaurant = new Restaurant();
	RestaurantSerializator ser = new RestaurantSerializator();

	JFrame jFrame;
	JPanel panel1, panel2,panel3, panel4, panel5,panel6, panel7,panel8;
	JLabel labelProdus,labelAdministrator,labelInstr,optiuniAdministrator, labelStergere, labelEditare, infoStergere,infoEditare;
	JButton addNewMenuItem,editMenuItem,deleteMenuItem, viewAllMenuItem;
	JTextField introducereProdus,editareProdus,stergereProdus;

	public AdministratorGraphicalUserInterface() {

		labelAdministrator = new JLabel(" Ati intrat in meniul administratorului!");
		addNewMenuItem = new JButton("addNewMenuItem");
		editMenuItem = new JButton("editMenuItem");
		deleteMenuItem = new JButton("deleteMenuItem");
		viewAllMenuItem = new JButton("viewAllMenuItem");
		introducereProdus = new JTextField(20);
		editareProdus = new JTextField(20);
		stergereProdus = new JTextField(20);
		

		optiuniAdministrator = new JLabel(" Optiunile Administratorului sunt urmatoarele ");
		labelProdus=new JLabel("Indroduceti produsul:");
		labelInstr=new JLabel("(Ex:nume,pret,compozitie<optional>)");
		infoStergere=new JLabel("(Ex:nume)");
		infoEditare=new JLabel("(Ex:nume,pret)");
		labelStergere=new JLabel("Stergeti un produs:");
		labelEditare=new JLabel("Editati un produs:");
		panel8=new JPanel();
		panel7=new JPanel();
        panel6=new JPanel();
		panel5 = new JPanel();
		panel4 = new JPanel();
		panel3 = new JPanel();
		panel2 = new JPanel();
		panel1 = new JPanel();

		panel2.add(addNewMenuItem);
		panel2.add(editMenuItem);
		panel2.add(deleteMenuItem);
		panel2.add(viewAllMenuItem);

		panel4.add(labelAdministrator);
		panel1.add(optiuniAdministrator);
		panel5.add(labelProdus);
		panel5.add(introducereProdus);
		panel5.add(labelInstr);
		panel7.add(labelEditare);
		panel7.add(editareProdus);
		panel7.add(infoEditare);
		panel8.add(labelStergere);
		panel8.add(stergereProdus);
		panel8.add(infoStergere);
		

		panel3.add(panel4);
		panel3.add(panel1);
		panel3.add(panel2);
		panel3.add(panel5);
		panel3.add(panel6);
        panel3.add(panel7);
        panel3.add(panel8);
		panel5.setLayout(new FlowLayout());
		panel7.setLayout(new FlowLayout());
		panel8.setLayout(new FlowLayout());
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));

		jFrame = new JFrame("Administrator");
		jFrame.add(panel3);
		jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		jFrame.setSize(1000, 500);
		jFrame.setVisible(true);

		action();
	}

	public void action() {
		addNewMenuItem.addActionListener(this);
		editMenuItem.addActionListener(this);
		deleteMenuItem.addActionListener(this);
		viewAllMenuItem.addActionListener(this);
	}

	public void add() {
		String s = introducereProdus.getText();
		ArrayList<MenuItem> items = new ArrayList<MenuItem>();

		ArrayList<String> comp = new ArrayList<String>();
		String[] componente = s.split(",");
		for (int i = 0; i < componente.length; i++) {
			comp.add(componente[i]);

		}
		if (comp.size() == 2) {
			BaseProduct prod = new BaseProduct();
			prod.setPretBaseProd(Double.parseDouble(comp.get(1)));
			prod.setNumeBaseProd(comp.get(0));

			restaurant.createNewMenuItem(prod);
		}

		else {
			ArrayList<MenuItem> produseCautate = ser.deserializationMenuItem();
			CompositeProduct prod = new CompositeProduct();
			prod.setPretCompProd(Double.parseDouble(comp.get(1)));
			prod.setNumeCompProd(comp.get(0));
			for (int i = 2; i < comp.size(); i++) {

				for (int j = 0; j < produseCautate.size(); j++) {
					if (produseCautate.get(j) instanceof BaseProduct)
						if (((BaseProduct) produseCautate.get(j)).getNumeBaseProd().compareTo(comp.get(i)) == 0)
							{((CompositeProduct) prod).addItems(produseCautate.get(j));
					prod.addItems(produseCautate.get(j));}
				}
				for (int j = 0; j < produseCautate.size(); j++) {

					if (produseCautate.get(j) instanceof CompositeProduct)
						if (((CompositeProduct) produseCautate.get(j)).getNumeCompProd().compareTo(comp.get(i)) == 0)
							{((CompositeProduct) prod).addItems(produseCautate.get(j));
					prod.addItems(produseCautate.get(j));}
				}
			}

			restaurant.createNewMenuItem(prod);

		}

	}

	public void delete() {
		String s = stergereProdus.getText();
		restaurant.deleteMenuItem(s);

	}

	private void edit() {
		String s = editareProdus.getText();
		String[] componente = s.split(",");
		String nume = componente[0];
		double pret = Double.parseDouble(componente[1]);

		restaurant.editMenuItem(nume, pret);

	}

	public void view() {

		String[] nume =new String[20];
		String[] pret =new String[20];
		String[][] data = new String[20][4];

		int i1 = 0;

		ArrayList<MenuItem> items;
		items = ser.deserializationMenuItem();
		restaurant.setItems(items);

		for (int i = 0; i < items.size(); i++) {
			if (items.get(i) instanceof BaseProduct) {
				
				nume[i] = ((BaseProduct) items.get(i)).getNumeBaseProd();
				pret[i] = "" + ((BaseProduct) items.get(i)).getPretBaseProd();
				data[i1][0] = nume[i];
				data[i1][1] = pret[i]+" RON";
				data[i1][2] = "baseProduct";
				data[i1][3]="-";
				i1++;
			}
			
			if (items.get(i) instanceof CompositeProduct) {
				nume[i] = ((CompositeProduct) items.get(i)).getNumeCompProd();
				pret[i] = "" + ((CompositeProduct) items.get(i)).getPretCompProd();
				data[i1][0] = nume[i];
				data[i1][1] = pret[i]+" RON";
				data[i1][2] = "compositeProduct";
				ArrayList<MenuItem> prodBase=((CompositeProduct) items.get(i)).getItem();
				data[i1][3]="";
				/*for(int a=0;a<prodBase.size();a++) {
					if(prodBase.get(a) instanceof BaseProduct)
					data[i1][3]=data[i1][3]+" "+((BaseProduct) prodBase.get(a)).getNumeBaseProd();
				*/
			i1++;
		}
		}
			
		JTable table;
		setLayout(new FlowLayout());
		String[] column = { "nume", "pret","specificatie"};
		table = new JTable(data, column);
		table.setPreferredScrollableViewportSize(new Dimension(450, 63));

		table.setFillsViewportHeight(true);

		JScrollPane jps = new JScrollPane(table);
		add(jps);
		JFrame jf = new JFrame("Table");

		jf.setSize(500, 500);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(1);
		jf.add(table);

	}
	
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == addNewMenuItem) {
			add();
		}
		if (e.getSource() == editMenuItem) {
			edit();

		}
		if (e.getSource() == deleteMenuItem) {
			delete();

		}
		if (e.getSource() == viewAllMenuItem) {
			view();

		}

	}

}
