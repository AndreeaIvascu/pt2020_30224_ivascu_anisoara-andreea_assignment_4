package bussinesLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable{
     
	private int orderID;
	private int dateHour;
	private int dateMin;
	private int table;

	private ArrayList<MenuItem> item;
	
	
	
	public Order(int orderID, int dateHour, int dateMin,int table) {
	
		this.orderID = orderID;
		this.dateHour = dateHour;
		this.dateMin = dateMin;
		this.setTable(table);
		
		this.item = new ArrayList<MenuItem>();
	}
	public Order() {
		// TODO Auto-generated constructor stub
	}
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public int getDateHour() {
		return dateHour;
	}
	public void setDateHour(int dateHour) {
		this.dateHour = dateHour;
	}
	public int getDateMin() {
		return dateMin;
	}
	public void setDateMin(int dateMin) {
		this.dateMin = dateMin;
	}
	
	public ArrayList<MenuItem> getItem() {
		return item;
	}
	public void setItem(ArrayList<MenuItem> item) {
		this.item = item;
	}
	
	public void addItems(MenuItem item)
	{
		this.item.add(item);
	}
	
	public int hashCode()
	{
		return table;
	}
	
	public double getPrice()
	{
		
		double price=0;
		

		for (int i = 0; i < item.size(); i++) {
			
			price=price+item.get(i).computePrice();
			
			/*if (item.get(i) instanceof BaseProduct)
				price = price + ((BaseProduct) item.get(i)).getPretBaseProd();
			if (item.get(i) instanceof CompositeProduct)
				price = price + ((CompositeProduct) item.get(i)).getPretCompProd();*/
		}
		
		return price;
		
	}
	public int getTable() {
		return table;
	}
	public void setTable(int table) {
		this.table = table;
	}
	
	
	
}
