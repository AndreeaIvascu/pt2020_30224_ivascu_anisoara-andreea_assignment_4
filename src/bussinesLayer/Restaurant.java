package bussinesLayer;

import java.io.PrintWriter;
import java.util.*;

import dataLayer.*;

public class Restaurant extends Observable implements IRestaurantProcessing {

	private HashMap<Order, ArrayList<MenuItem>> table;
	private ArrayList<Order> orders;
	private ArrayList<MenuItem> items;

	RestaurantSerializator ser = new RestaurantSerializator();

	public Restaurant() {

		this.orders = new ArrayList<Order>();
		this.items = new ArrayList<MenuItem>();
		this.table = new HashMap<Order, ArrayList<MenuItem>>();
	}

	public boolean wellFormed() {
		if (this instanceof Restaurant) {
			return true;
		} else
			return false;
	}

	@Override
	public void createNewMenuItem(MenuItem item) {
		// TODO Auto-generated method stub

		// precondition
		assert item != null;
		assert wellFormed();

		try {

			items.add(item);
			ser.serializationMenuItem(items);
		} catch (Exception e) {
			System.out.println("Eroare create MenuItem");
		}

		// postcondition
		assert wellFormed();
		assert item != null;

	}

	@Override
	public void deleteMenuItem(String item) {
		// TODO Auto-generated method stub

		assert item != "";
		assert wellFormed();

		try {

			for (int i = 0; i < items.size(); i++) {
				if (items.get(i) instanceof BaseProduct) {
					if (((BaseProduct) items.get(i)).getNumeBaseProd().compareTo(item) == 0) {
						items.remove(items.get(i));
						ser.serializationMenuItem(items);
					}
				}
				if (items.get(i) instanceof CompositeProduct) {
					if (((CompositeProduct) items.get(i)).getNumeCompProd().compareTo(item) == 0) {
						items.remove(items.get(i));
						ser.serializationMenuItem(items);
					}
				}

			}

		} catch (Exception e) {
			System.out.println("Eroare delete MenuItem");
		}

		// postcondition
		assert wellFormed();
		assert item != "";

	}

	@Override
	public void editMenuItem(String item, double pret) {
		// TODO Auto-generated method stub

		assert item != "";
		assert wellFormed();

		try {

			for (int i = 0; i < items.size(); i++) {
				if (items.get(i) instanceof BaseProduct) {
					if (((BaseProduct) items.get(i)).getNumeBaseProd().compareTo(item) == 0)
						((BaseProduct) items.get(i)).setPretBaseProd(pret);
					ser.serializationMenuItem(items);
				}
				if (items.get(i) instanceof CompositeProduct) {
					if (((CompositeProduct) items.get(i)).getNumeCompProd().compareTo(item) == 0)
						((CompositeProduct) items.get(i)).setPretCompProd(pret);
					ser.serializationMenuItem(items);
				}

			}

		} catch (Exception e) {
			System.out.println("Eroare edit MenuItem!");
		}

		// postcondition
		assert wellFormed();
		assert item != "";

	}

	@Override
	public void createNewOrder(Order order) {
		// TODO Auto-generated method stub

		// precondition
		assert order != null;
		assert wellFormed();

		try {

			table.put(order, items);
			orders.add(order);

			ser.serializationOrders(orders);
			setChanged();
			notifyObservers(order.getItem());

		} catch (Exception e) {
			System.out.println("Eroare create Order!");
		}

		// postcondition
		assert wellFormed();
		assert order != null;
	}

	@Override
	public void computePrice(Order order) {
		// TODO Auto-generated method stub
		
		double price=0;
		price=order.getPrice();

	}

	@Override
	public void generateBill(Order order) {
		// TODO Auto-generated method stub

		FileWriter f=new FileWriter();
		
		f.chitanta(order);
		

	}

	public ArrayList<MenuItem> getItems() {
		return items;
	}

	public void setItems(ArrayList<MenuItem> items) {
		this.items = items;
	}

	public ArrayList<Order> getOrders() {
		return orders;
	}

	public void setOrders(ArrayList<Order> orders) {
		this.orders = orders;
	}

}
