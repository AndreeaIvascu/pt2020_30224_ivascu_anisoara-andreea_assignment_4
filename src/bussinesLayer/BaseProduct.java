package bussinesLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable{
     
	private String numeBaseProd;
	private double pretBaseProd;
	public BaseProduct(String numeBaseProd, double pretBaseProd) {
		
		this.numeBaseProd = numeBaseProd;
		this.pretBaseProd = pretBaseProd;
	}
	public BaseProduct() {
		// TODO Auto-generated constructor stub
	}
	public String getNumeBaseProd() {
		return numeBaseProd;
	}
	public void setNumeBaseProd(String numeBaseProd) {
		this.numeBaseProd = numeBaseProd;
	}
	
	public double getPretBaseProd() {
		return pretBaseProd;
	}
	public void setPretBaseProd(double pretBaseProd) {
		this.pretBaseProd = pretBaseProd;
	}
	@Override
	public double computePrice() {
		// TODO Auto-generated method stub
		return pretBaseProd;
	}
	
	public String toString()
	{  String s="";
	  s=numeBaseProd+" "+pretBaseProd;
		return s;
	}
	

}
