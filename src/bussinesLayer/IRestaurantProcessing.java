package bussinesLayer;

import java.util.ArrayList;

public interface IRestaurantProcessing {
	
	public void createNewMenuItem(MenuItem item);
	public void deleteMenuItem(String item);
	public void editMenuItem(String item,double pret);
	
	public void createNewOrder(Order order);
	public void computePrice(Order order);
	public void generateBill(Order order);
	

}
