package bussinesLayer;

import java.io.Serializable;
import java.util.*;

public class CompositeProduct extends MenuItem implements Serializable {

	private String numeCompProd;
	private double pretCompProd;
	private ArrayList<MenuItem> item=new ArrayList<MenuItem> ();

	public CompositeProduct(String numeCompProd,  double pretCompProd) {

		this.numeCompProd = numeCompProd;
		this.pretCompProd = pretCompProd;
		this.item = new ArrayList<MenuItem>();
	}

	public CompositeProduct() {
		// TODO Auto-generated constructor stub
	}

	public String getNumeCompProd() {
		return numeCompProd;
	}

	public void setNumeCompProd(String numeCompProd) {
		this.numeCompProd = numeCompProd;
	}

	public double getPretCompProd() {
		return pretCompProd;
	}

	public void setPretCompProd(double pretCompProd) {
		this.pretCompProd = pretCompProd;
	}

	public ArrayList<MenuItem> getItem() {
		return item;
	}

	public void setItem(ArrayList<MenuItem> item) {
		this.item = item;
	}

	public void addItems(MenuItem item) {
		this.item.add(item);
	}

	@Override
	public double computePrice() {
		// TODO Auto-generated method stub
		return pretCompProd;
	}

	public String toString() {
		String s = "";
		s = numeCompProd + " " + pretCompProd;
		return s;

	}

	public ArrayList<BaseProduct> afisBaseProd() {

		ArrayList<BaseProduct> baseProd = new ArrayList<BaseProduct>();

		for (int i = 0; i < this.item.size(); i++) {
			if (this.item.get(i) instanceof BaseProduct) {
				baseProd.add((BaseProduct) this.item.get(i));
			}
		}
		return baseProd;
	}

}
