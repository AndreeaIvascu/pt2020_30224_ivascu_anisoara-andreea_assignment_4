package bussinesLayer;

public abstract class MenuItem {
	
	public abstract double computePrice();

}
